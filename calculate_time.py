#!/usr/bin/env python3

import csv
import sys
from datetime import datetime, timedelta

def calculate_total_hours(filename, start_date, end_date):
    total_hours = 0.0

    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            _, _, action, timestamp = row[:4]
            if action == "clock-out":
                try:
                    hours_worked = float(row[4])
                except (IndexError, ValueError):
                    hours_worked = 0.0

                entry_date = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
                if start_date <= entry_date <= end_date:
                    total_hours += hours_worked

    return total_hours

def main():
    if len(sys.argv) != 2:
        print("Usage: python script.py <filename>")
        sys.exit(1)

    filename = sys.argv[1]
    user = filename.split('_')[0]  # Assuming the filename format is 'username_times.csv'

    print("Welcome to the time tracking analyzer for", user)

    try:
        start_date_input = input("Enter the start date (YYYY-MM-DD): ")
        end_date_input = input("Enter the end date (YYYY-MM-DD): ")

        start_date = datetime.strptime(start_date_input, "%Y-%m-%d")
        end_date = datetime.strptime(end_date_input, "%Y-%m-%d") + timedelta(days=1) - timedelta(seconds=1)

    except ValueError:
        print("Invalid date format. Please use YYYY-MM-DD format.")
        return

    total_hours = calculate_total_hours(filename, start_date, end_date)

    print(f"Total hours worked by {user} from {start_date.strftime('%Y-%m-%d %H:%M:%S')} to {end_date.strftime('%Y-%m-%d %H:%M:%S')}: {total_hours:.2f} hours")

if __name__ == "__main__":
    main()
