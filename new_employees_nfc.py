#!/usr/bin/env python3

import subprocess

def run_nfc_poll():
    try:
        # Run nfc-poll and capture its output
        result = subprocess.run(['nfc-poll'], capture_output=True, text=True)

        # Check if nfc-poll was successful
        if result.returncode == 0:
            output_lines = result.stdout.strip().split('\n')
            uid_line = None

            # Find the line with UID information
            for line in output_lines:
                if 'UID (NFCID1):' in line:
                    uid_line = line.strip()
                    break

            # Extract the UID without spaces
            if uid_line:
                uid = uid_line.split(':')[1].replace(" ", "").lower()
                return uid
            else:
                print("UID not found.")
        else:
            print("Error running nfc-poll.")
    except Exception as e:
        print(f"An error occurred: {e}")
    return None

def add_employee_to_users():
    name = input("Enter the employee's name: ")
    uid = run_nfc_poll()

    if uid:
        with open('users.txt', 'a') as f:
            f.write(f"{name} {uid}\n")
        print(f"{name} with UID {uid} added to users.txt.")

def main():
    print("Welcome to the new employee registration system!")
    add_employee_to_users()

if __name__ == "__main__":
    main()
