# Timeclock

Simple timeclock where users clock-in/clock-out and the timestamps are saved to an individual csv for each user. Those files are then synced to OneDrive or whatever cloud storage and shared with read-only access. One version is for a 4 digit PIN and another is for using NFC cards.