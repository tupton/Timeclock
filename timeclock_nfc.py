#!/usr/bin/env python3

import csv
import os
from datetime import datetime
import subprocess
import re
# Following prevents pygame support banner being displayed in output.
os.environ['PYGAME_HIDE_SUPPORT_PROMPT']='1'
import pygame

# Define user data file
USER_FILE = "users.txt"

# Initialize pygame mixer
pygame.mixer.init()

# Define the beep sound
beep_sound = pygame.mixer.Sound("beep.wav")  # Replace "beep.wav" with the path to your beep sound file

def get_nfc_uid():
    try:
        # Run the nfc-poll command and capture its output
        result = subprocess.run(['nfc-poll'], capture_output=True, text=True, check=True)
        output = result.stdout

        # Parse the output to extract the UID
        lines = output.splitlines()
        for line in lines:
            if "UID (NFCID1):" in line:
                uid = line.split(":")[1].strip()
                # Remove spaces from the UID using regular expression
                uid = re.sub(r'\s', '', uid)
                return uid

        # If UID not found, return None
        return None

    except subprocess.CalledProcessError:
        # Handle any errors that may occur when running nfc-poll
        return None

# Read user data
def read_users():
    with open(USER_FILE, 'r') as f:
        return [line.strip().split() for line in f]

def get_user_clock_file(user_name):
    # Create a file name based on the user's name
    return f"{user_name}_times.csv"

def write_clock_record(user, uid, action, shift_duration=None):
    clock_file = get_user_clock_file(user)
    with open(clock_file, 'a', newline='') as f:
        writer = csv.writer(f)
        if shift_duration is not None:
            # Convert timedelta to hours in decimal format
            hours_decimal = shift_duration.total_seconds() / 3600
            writer.writerow([user, uid, action, datetime.now(), "{:.2f}".format(hours_decimal)])
        else:
            writer.writerow([user, uid, action, datetime.now()])

def read_last_action(uid, users):
    # Find the user's name based on their UID
    user_name = next((user for user, user_uid in users if user_uid == uid), None)
    if user_name is None:
        return None, None

    clock_file = get_user_clock_file(user_name)
    # Check if the user's file exists
    if not os.path.exists(clock_file):
        return None, None

    with open(clock_file, 'r') as f:
        reader = csv.reader(f)
        last_action = None
        last_time = None
        for row in reader:
            if row[1] == uid:
                last_action = row[2]
                last_time = datetime.strptime(row[3], "%Y-%m-%d %H:%M:%S.%f")
        return last_action, last_time

def show_clocked_in_users(users):
    """
    Display users who are currently clocked in.
    """
    clocked_in_users = set()
    for user, _ in users:
        clock_file = get_user_clock_file(user)
        if os.path.exists(clock_file):
            with open(clock_file, 'r') as f:
                reader = csv.reader(f)
                for row in reader:
                    _, _, action, _ = row[:4]
                    if action == "clock-in":
                        clocked_in_users.add(user)
                    elif action == "clock-out" and user in clocked_in_users:
                        clocked_in_users.remove(user)

    if clocked_in_users:
        print("Currently clocked in users:")
        for user in clocked_in_users:
            print(user)
    else:
        print("No users are currently clocked in.")

# Main function
def main():
    users = read_users()

    print("Welcome to the time tracking system!")

    while True:
        uid = None

        while uid is None:
            uid = get_nfc_uid()

        # Check if the UID matches a user
        matched_user = None
        for user, user_uid in users:
            if uid == user_uid:
                matched_user = user
                break

        if matched_user:
            print(f"\nWelcome, {matched_user}!")

            last_action, last_time = read_last_action(uid, users)
            if last_action == "clock-in" and last_time is not None:
                duration = datetime.now() - last_time
                print(f"Clocked out successfully! Your shift duration was {duration.total_seconds() / 3600:.2f} hours")
                write_clock_record(matched_user, uid, "clock-out", duration)
                # Play a beep sound when clocking out
                beep_sound.play()
            elif last_action == "clock-out" or last_action is None:
                write_clock_record(matched_user, uid, "clock-in")
                print("Clocked in successfully!")
                # Play a beep sound when clocking in
                beep_sound.play()
            else:
                print("You need to clock-in first!")

        else:
            print("Card not associated with any user!")
            beep_sound.play()

if __name__ == "__main__":
    main()
